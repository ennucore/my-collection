import json
import time

import requests
from bs4 import BeautifulSoup

url = 'https://ru.ucoin.net/gallery/?uid=101954&order=pd'
headers = {
    "User-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36"}


def get_coins_links(url, headers):
    coins_links = []
    r0 = requests.get(url, headers=headers)
    r0 = r0.text
    soup = BeautifulSoup(r0, 'lxml')
    try:
        pages_number = int((soup.find('div', class_='pages').find_all('a')[-1].text))
    except:
        pages_number = 1

    pages_number = 30

    for page in range(pages_number):
        r = requests.get(url + '&page=' + str(page + 1), headers=headers)
        r = r.text
        soup = BeautifulSoup(r, 'lxml')
        coins_classes = soup.find_all('div', class_='coin')
        for coin_class in coins_classes:
            coins_links.append('https://ru.ucoin.net' + coin_class.find('a', class_='blue-15').get('href'))
            time.sleep(1)

        print('page', page + 1, 'from', pages_number)

    return coins_links


coins_links = (get_coins_links(url, headers))


def get_all_information(coins_links, headers):
    coins_information = {}

    for coin_link in coins_links:

        coin = ''
        coin_price_data = {}
        coin_information = {}

        r = requests.get(coin_link, headers=headers)
        r = r.text
        soup = BeautifulSoup(r, 'lxml')

        try:
            name = soup.find('div', style='clear:both;').find('h1').text
        except:
            name = ''

        '''res = soup.find('div', id='chart_div')
        res2 = res.find('tbody').find_all('tr')
        coin_price_data.update({'date':  tr.find_all('td')[0].text,
                                'price': tr.find_all('td')[1].text})
        print(coin_price_data)'''

        try:
            obverse_link = soup.find('table', class_='coin-img').find_all('a')[0].get('href')
            reverse_link = soup.find('table', class_='coin-img').find_all('a')[1].get('href')
        except:
            obverse_link = ''
            reverse_link = ''

        try:
            extra_name = soup.find('div', style='clear:both;').find('h3').text
        except:
            extra_name = ''

        try:
            price = soup.find('a', class_='gray-12 right pricewj').find('span').text.replace(',', '')
        except:
            price = '0.00'

        coin_information.update(
            {'цена:': price, 'link:': coin_link})

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[0]).find_all('tr'):
                coin_information.update({tr.find('th').text: tr.find('td').text})
        except:
            pass

        try:
            for tr in (soup.find_all('table', class_='tbl coin-info')[1]).find_all('tr'):
                coin_information.update({tr.find('th').text: tr.find('td').text})
        except:
            pass

        coin = name + ' ' + extra_name

        try:
            img_obverse = requests.get(obverse_link, headers=headers).content
            with open('obverses/' + name + '|obverse' + '.jpg', 'wb') as handler:
                handler.write(img_obverse)
        except:
            pass

        try:
            img_reverse = requests.get(reverse_link, headers=headers).content
            with open('reverses/' + name + '|reverse' + '.jpg', 'wb') as handler:
                handler.write(img_reverse)
        except:
            pass

        coins_information.update({coin: coin_information})

    with open('coins_information.json', 'w') as f:
        json.dump(coins_information, f, ensure_ascii=False)


get_all_information(coins_links, headers)
